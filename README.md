BEdita
======

This role's duty is to set up a [BEdita](http://www.bedita.com) instance.

Variables
---------

### `bedita_version`
The BEdita version to clone. Can be either a branch name, a tag name, or a commit hash. By default, this is `3-corylus`.

### `bedita_root`
The directory where the BEdita instance will be cloned. By default, this is `/var/www/bedita`.

### `bedita_project`
The project name of the BEdita instance. By default, this is `BEdita CMS`.

### `bedita_debug`
The debug level in the backend application. Can be either `0`, `1` or `2`. By default, this is set to `0`.

### `bedita_url`
The main URL of the backend application, **without** a trailing slash. By default, this is the same as the FQDN of the machine, via HTTP.

### `bedita_media`
The media URL of the whole BEdita instance, **without** a trailing slash. By default, this is the subfolder `files` of `bedita_url`.

### `bedita_db`
The connection settings to the database. It is a dictionary that consists of:

- a **required** key `host`, that represents the host of the MySQL server
- a **required** key `username`
- a **required** key `password`
- a **required** key `database`

### `bedita_init`
Boolean flag to set whether the database should be initialized if no tables are found. By default, this option is turned off.

Example
-------

```yaml
---
bedita_version: 3-corylus
bedita_root: /var/www/bedita
bedita_project: BEdita CMS
bedita_debug: 0
bedita_url: http://bedita.example.com
bedita_media: http://static.example.com
bedita_db:
  host: localhost
  username: bedita
  password: "B3D!T4"
  database: bedita
bedita_init: yes
```
