<?php
/**
 * {{ ansible_managed }}
 */

class DATABASE_CONFIG
{
    public $default = array(
        'driver' => 'mysql',
        'persistent' => false,
        'connect' => 'mysql_connect',
        'host' => '{{ bedita_db.host }}',
        'login' => '{{ bedita_db.username }}',
        'password' => '{{ bedita_db.password }}',
        'database' => '{{ bedita_db.database }}',
        'schema' => '',
        'prefix' => '',
        'encoding' => 'utf8'
    );
}
