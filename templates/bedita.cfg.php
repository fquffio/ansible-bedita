<?php
/**
 * {{ ansible_managed }}
 */

{% if bedita_project is defined %}
// BEdita instance name
$config['projectName'] = '{{ bedita_project }}';
{% endif %}

// BEdita URL
$config['beditaUrl'] = '{{ bedita_url }}';

// Multimedia - root folder on filesystem
$config['mediaRoot'] = BEDITA_CORE_PATH . DS . 'webroot' . DS . 'files';

// Multimedia - URL prefix (without trailing slash)
$config['mediaUrl'] = '{{ bedita_media }}';
